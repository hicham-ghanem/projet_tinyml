**Projet de classification d'images pour la détection du cancer de la peau**

Ce projet vise à utiliser Arduino, la carte Nano 33 Sense BLE et le module caméra pour effectuer une classification binaire d'images pour détecter un cancer de la peau. TensorFlow Lite sera utilisé pour entraîner et produire le modèle d'inférence, tandis qu'Edge Impulse sera utilisé pour entraîner le modèle.

**Prérequis**

Carte Nano 33 Sense BLE

Module caméra

Arduino

Edge Impulse

TensorFlow Lite

Google Colab

Petite quantité d'images de cancer de la peau et d'images sans cancer pour entraîner le modèle

**Etapes**

Recueillir des images.
Entraîner et produire le modèle d'inférence avec Edge Impulse.
Configurer la carte Nano 33 Sense BLE avec Arduino.
Charger le modèle TensorFlow Lite sur la carte Nano 33 Sense BLE.
Exécuter l'inférence sur les images capturées.
Afficher les résultats sur le terminal série et faire clignoter la LED si nécessaire.
Tester le modèle sur différentes images.

**Résultat attendu**

Un modèle de classification d'images fonctionnel pour détecter un cancer de la peau qui affiche les résultats sur le terminal série et fait clignoter la LED si nécessaire.

**Résultats** 

Ce projet a été réalisé sur edge impulse. Tout d'abord j'ai fait l'acquisition de données avec des photos de peau cancéreuse et des photos de ma peau saine . Puis j'ai réalise l'impulse design ci-dessous : 
![image](https://user-images.githubusercontent.com/86970472/216790098-e1ccec03-f8f3-44eb-b844-2fa699dfda29.png)

Voici l'archictecture neuronal du modéle et les résultats de l'entrainement : 

![image](https://user-images.githubusercontent.com/86970472/216790408-f0e627da-713e-4c96-aea6-efa5fcba83ff.png) ![image](https://user-images.githubusercontent.com/86970472/216790428-15f68b50-c07f-4c02-b3e6-827ceeeccf9e.png)

On voit que les résultats sont bons. Puis je teste en live mon modèle avec mon téléphone : 
![image](https://user-images.githubusercontent.com/86970472/216790664-a4601df5-35fd-4548-b3b9-921bee7d280c.png)

Les résultats sont très satisfaisants. 

**Points négatifs**

Le but après cela était de télécharger une librairie pour tester le modèle sur l'arduino avec le module caméra mais malheureusement la qualité d'image est très mauvaise ce qui fausse les résultats. 
